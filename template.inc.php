<?php
/**
 * Price Lists - Template
 *
 * @package Coordinator\Modules\Price-Lists
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // build html object
 $html=new cHTML($module_name);
 // build nav object
 $nav=new cNav("nav-tabs");
 $nav->setTitle(api_text(MODULE));
 // dashboard
 $nav->addItem(api_icon("fa-th-large",null,"hidden-link"),"?mod=".MODULE."&scr=dashboard");
 // lists
 if(substr(SCRIPT,0,5)=="lists"){
  $nav->addItem(api_text("nav-lists-list"),"?mod=".MODULE."&scr=lists_list");
  // operations
  if($list_obj->id && in_array(SCRIPT,array("lists_view","lists_edit"))){
   $nav->addItem(api_text("nav-operations"),null,null,"active");
   $nav->addSubItem(api_text("nav-lists-operations-edit"),"?mod=".MODULE."&scr=lists_edit&idList=".$list_obj->id,(api_checkAuthorization("lists-edit")));
  }else{$nav->addItem(api_text("nav-lists-add"),"?mod=".MODULE."&scr=lists_edit",(api_checkAuthorization("lists-edit")));}
 }
 // add nav to html
 $html->addContent($nav->render(false));
?>