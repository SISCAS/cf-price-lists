<?php
/**
 * Price Lists - List
 *
 * @package Coordinator\Modules\Price-Lists
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

/**
 * Price Lists List class
 */
class cPriceListsList{

 /** Properties */
 protected $id;
 protected $name;
 protected $description;
 protected $addTimestamp;
 protected $addFkUser;
 protected $updTimestamp;
 protected $updFkUser;
 protected $deleted;

 /**
  * Debug
  *
  * @return object List object
  */
 public function debug(){return $this;}

 /**
  * List class
  *
  * @param mixed $list List object or ID
  */
 public function __construct($list){
  // get object
  if(is_numeric($list)){$list=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `price-lists__lists` WHERE `id`='".$list."'",$GLOBALS['debug']);}
  if(!$list->id){return false;}
  // set properties
  $this->id=(int)$list->id;
  $this->name=stripslashes($list->name);
  $this->description=stripslashes($list->description);
  $this->addTimestamp=(int)$list->addTimestamp;
  $this->addFkUser=(int)$list->addFkUser;
  $this->updTimestamp=(int)$list->updTimestamp;
  $this->updFkUser=(int)$list->updFkUser;
  $this->deleted=(int)$list->deleted;
 }

 /**
  * Get
  *
  * @param string $property Property name
  * @return string Property value
  */
 public function __get($property){return $this->$property;}

 /**
  * Get Events
  */
 public function getEvents($debug=false){
  // definitions
  $events_array=array();
  // make query where
  if($GLOBALS['debug'] || $debug){$query_where="1";}else{$query_where="`level`<>'debug'";}
  // get list events
  $events_results=$GLOBALS['database']->queryObjects("SELECT * FROM `price-lists__events` WHERE ".$query_where." AND `fkList`='".$this->id."' ORDER BY `timestamp` DESC",$GLOBALS['debug']);
  foreach($events_results as $event){$events_array[$event->id]=new cEvent($event,"price-lists");}
  // return
  return $events_array;
 }

}
?>