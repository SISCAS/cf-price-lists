<?php
/**
 * Price Lists - Lists List
 *
 * @package Coordinator\Modules\Price-Lists
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("lists-view","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // definitions
 $users_array=array();
 // set html title
 $html->setTitle(api_text("lists_list"));
 // definitions
 $lists_array=array();
 // build filter
 $filter=new cFilter();
 $filter->addSearch(array("name","description"));
 // build query object
 $query=new cQuery("price-lists__lists",$filter->getQueryWhere());
 $query->addQueryOrderField("name");
 // build pagination object
 $pagination=new cPagination($query->getRecordsCount());
 // cycle all results
 foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$lists_array[$result_f->id]=new cPriceListsList($result_f);}
 // build table
 $table=new cTable(api_text("lists_list-tr-unvalued"));
 $table->addHeader($filter->link(api_icon("fa-filter"),api_text("filters-modal-link"),"hidden-link"),null,16);
 $table->addHeader(api_text("lists_list-th-name"),"nowrap");
 $table->addHeader(api_text("lists_list-th-description"),null,"100%");
 $table->addHeader("&nbsp;",null,16);
 // cycle all lists
 foreach($lists_array as $list_obj){
  // build operation button
  $ob=new cOperationsButton();
  $ob->addElement("?mod=".MODULE."&scr=lists_edit&idList=".$list_obj->id."&return_scr=lists_list","fa-pencil",api_text("lists_list-td-edit"));
  if($list_obj->deleted){$ob->addElement("?mod=".MODULE."&scr=submit&act=list_undelete&idList=".$list_obj->id,"fa-trash-o",api_text("lists_list-td-undelete"),true,api_text("lists_list-td-undelete-confirm"));}
  else{$ob->addElement("?mod=".MODULE."&scr=submit&act=list_delete&idList=".$list_obj->id,"fa-trash",api_text("lists_list-td-delete"),true,api_text("lists_list-td-delete-confirm"));}
  // make table row class
  $tr_class_array=array();
  if($list_obj->id==$_REQUEST['idList']){$tr_class_array[]="info";}
  if($list_obj->deleted){$tr_class_array[]="deleted";}
  // build list row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowField(api_link("?mod=".MODULE."&scr=lists_view&idList=".$list_obj->id,api_icon("fa-search",null,"hidden-link"),api_text("lists_list-td-view")));
  $table->addRowField($list_obj->name,"nowrap");
  $table->addRowField($list_obj->description,"truncate-ellipsis");
  $table->addRowField($ob->render(),"text-right");
 }
 // build grid object
 $grid=new cGrid();
 $grid->addRow();
 $grid->addCol($filter->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($table->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($pagination->render(),"col-xs-12");
 // add content to html
 $html->addContent($grid->render());
 // renderize html
 $html->render();
 // debug
 api_dump($query,"query");
 api_dump($query->getQuerySQL(),"query sql");
?>