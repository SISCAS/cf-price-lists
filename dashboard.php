<?php
/**
 * Price Lists - Dashboard
 *
 * @package Coordinator\Modules\Price-Lists
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set html title
 $html->setTitle(api_text(MODULE));
 // build dashboard object
 $dashboard=new cDashboard();
 $dashboard->addTile("?mod=".MODULE."&scr=lists_list",api_text("dashboard-lists-list"),api_text("dashboard-lists-list-description"),(api_checkAuthorization("price-lists-view")),"1x1","fa-book");
 $dashboard->addTile("?mod=".MODULE."&scr=lists_edit",api_text("dashboard-lists-add"),api_text("dashboard-lists-add-description"),(api_checkAuthorization("price-lists-edit")),"1x1","fa-file-text-o");
  // build grid object
 $grid=new cGrid();
 $grid->addRow();
 $grid->addCol($dashboard->render(),"col-xs-12");
 // add content to html
 $html->addContent($grid->render());
 // renderize html page
 $html->render();
?>