<?php
/**
 * Price Lists Functions
 *
 * @package Coordinator\Modules\Price-Lists
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

// include classes
require_once(ROOT."modules/price-lists/classes/cPriceListsList.class.php");

/**
 * Price Lists - Event Save
 *
 * @param object|integer $list List object or ID
 * @param string $event Event to log
 * @param string $level Level [information|warning|error|debug]
 * @param string $note Note
 * @return boolean true or false
 */
function api_priceLists_event_save($list,$event,$level="information",$note=null){ /* @todo valutare se fare api */
 // check parameters
 $list_obj=new cPriceListsList($list);
 //if(!($list_obj instanceof cPriceListsList)){return false;}
 if(!$list_obj->id){return false;}
 if(!$event){return false;}
 // build event query objects
 $event_qobj=new stdClass();
 $event_qobj->fkList=$list_obj->id;
 if($GLOBALS['session']->user->id){$event_qobj->fkUser=$GLOBALS['session']->user->id;}
 $event_qobj->timestamp=time();
 $event_qobj->event=$event;
 $event_qobj->level=$level;
 $event_qobj->note=$note;
 // debug
 api_dump($event_qobj,"event query object");
 // insert event
 $event_qobj->id=$GLOBALS['database']->queryInsert("price-lists__events",$event_qobj);
 // check and return
 if($event_qobj->id){return true;}
 else{return false;}
}

?>