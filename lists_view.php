<?php
/**
 * Price Lists - Lists View
 *
 * @package Coordinator\Modules\Price-Lists
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("lists-view","dashboard");
 // get objects
 $list_obj=new cPriceListsList($_REQUEST['idList']);
 // check objects
 if(!$list_obj->id){api_alerts_add(api_text("price-lists_alert-listNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=lists_list");}
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set html title
 $html->setTitle(api_text("lists_view",$list_obj->name));
 // check for tab
 if(!defined(TAB)){define("TAB","informations");}
 // build list description list
 $list_dl=new cDescriptionList("br","dl-horizontal");
 $list_dl->addElement(api_text("lists_view-dt-name"),api_tag("strong",$list_obj->name));
 $list_dl->addElement(api_text("lists_view-dt-description"),nl2br($list_obj->description));
 // build informations description list
 $information_dl=new cDescriptionList("br","dl-horizontal");
 // build tabs
 $tab=new cTab();
 $tab->addItem(api_icon("fa-bookmark")." ".api_text("lists_view-tab-informations"),$information_dl->render(),("informations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-text-o")." ".api_text("lists_view-tab-events"),api_events_table($list_obj->getEvents())->render(),("events"==TAB?"active":null));
 // build grid object
 $grid=new cGrid();
 $grid->addRow();
 $grid->addCol($list_dl->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($tab->render(),"col-xs-12");
 // add content to html
 $html->addContent($grid->render());
 // renderize html page
 $html->render();
 // debug
 api_dump($list_obj,"list");
?>