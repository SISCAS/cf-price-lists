<?php
/**
 * Price Lists
 *
 * @package Coordinator\Modules\Price-Lists
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 $module_name="price-lists";
 $module_repository_version_url="https://bitbucket.org/SISCAS/cf-price-lists/raw/master/VERSION.txt";
?>