<?php
/**
 * Price Lists - Submit
 *
 * @package Coordinator\Modules\Price-Lists
 * @list Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
// check for actions
if(!defined('ACTION')){die("ERROR EXECUTING SCRIPT: The action was not defined");}
// switch action
switch(ACTION){
 // lists
 case "list_save":list_save();break;
 case "list_delete":list_deleted(true);break;
 case "list_undelete":list_deleted(false);break;
 case "list_remove":list_remove();break;
 // default
 default:
  api_alerts_add(api_text("alert_submitFunctionNotFound",array(MODULE,SCRIPT,ACTION)),"danger");
  api_redirect("?mod=".MODULE);
}

/**
 * List Save
 */
function list_save(){
 api_dump($_REQUEST,"_REQUEST");
 // check authorizations
 api_checkAuthorization("lists-edit","dashboard");
 // get list object
 $list_obj=new cPriceListsList($_REQUEST['idList']);
 api_dump($list_obj,"list object");
 // build list query object
 $list_qobj=new stdClass();
 $list_qobj->id=$list_obj->id;
 $list_qobj->name=addslashes($_REQUEST['name']);
 $list_qobj->description=addslashes($_REQUEST['description']);
 // check for update
 if($list_obj->id){
  // update list
  $list_qobj->updTimestamp=time();
  $list_qobj->updFkUser=$GLOBALS['session']->user->id;
  api_dump($list_qobj,"list query object");
  // execute query
  $GLOBALS['database']->queryUpdate("price-lists__lists",$list_qobj);
  // log event
  api_priceLists_event_save($list_qobj,"listUpdated");
  // alert
  api_alerts_add(api_text("price-lists_alert-listUpdated"),"success");
 }else{
  // insert list
  $list_qobj->addTimestamp=time();
  $list_qobj->addFkUser=$GLOBALS['session']->user->id;
  api_dump($list_qobj,"list query object");
  // execute query
  $list_qobj->id=$GLOBALS['database']->queryInsert("price-lists__lists",$list_qobj);
  // log event
  api_priceLists_event_save($list_qobj,"listCreated");
  // alert
  api_alerts_add(api_text("price-lists_alert-listCreated"),"success");
 }
 // redirect
 api_redirect("?mod=".MODULE."&scr=".api_return_script("lists_view")."&idList=".$list_qobj->id);
}

/**
 * List Deleted
 *
 * @param boolean $deleted Deleted or Undeleted
 */
function list_deleted($deleted){
 api_dump($_REQUEST,"_REQUEST");
 // check authorizations
 api_checkAuthorization("lists-edit","dashboard");
 // get list object
 $list_obj=new cPriceListsList($_REQUEST['idList']);
 api_dump($list_obj,"list object");
 // check object
 if(!$list_obj->id){api_alerts_add(api_text("price-lists_alert-listNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=lists_list");}
 // build list query objects
 $list_qobj=new stdClass();
 $list_qobj->id=$list_obj->id;
 $list_qobj->deleted=($deleted?1:0);
 $list_qobj->updTimestamp=time();
 $list_qobj->updFkUser=$GLOBALS['session']->user->id;
 api_dump($list_obj,"list object");
 // update list
 $GLOBALS['database']->queryUpdate("price-lists__lists",$list_qobj);
 // make action
 if($deleted){$action="listDeleted";}else{$action="listUndeleted";}
 // log event
 api_priceLists_event_save($list_obj,$action,"warning");
 // redirect
 api_alerts_add(api_text("price-lists_alert-".$action),"warning");
 api_redirect("?mod=".MODULE."&scr=lists_list&idList=".$list_obj->id);
}

 /**
  * List Remove
  */
 function list_remove(){
  api_dump($_REQUEST,"_REQUEST");
  // disable function
  die("SUBMIT FUNCTION DISABLED");
  // check authorizations
  /*
  api_checkAuthorization("lists-edit","dashboard");
  // get list object
  $list_obj=new cPriceListsList($_REQUEST['idList']);
  api_dump($list_obj,"list object");
  // check object
  if(!$list_obj->id){api_alerts_add(api_text("price-lists_alert-listNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=lists_list");}
  // delete list
  $GLOBALS['database']->queryDelete("price-lists__lists",$list_obj->id);
  // @todo save event in framework
  // redirect
  api_alerts_add(api_text("price-lists_alert-listRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=lists_list");
  */
 }

             ?>