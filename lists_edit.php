<?php
/**
 * Price Lists - Lists Edit
 *
 * @package Coordinator\Modules\Price-Lists
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("lists-edit","dashboard");
 // get objects
 $list_obj=new cPriceListsList($_REQUEST['idList']);
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set html title
 $html->setTitle(($list_obj->id?api_text("lists_edit",$list_obj->name):api_text("lists_edit-add")));
 // build list form
 $form=new cForm("?mod=".MODULE."&scr=submit&act=list_save&idList=".$list_obj->id."&return_scr=".$_REQUEST['return_scr'],"POST",null,"lists_edit");
 $form->addField("text","name",api_text("lists_edit-ff-name"),$list_obj->name,api_text("lists_edit-ff-name-placeholder"),null,null,null,"required");
 $form->addField("textarea","description",api_text("lists_edit-ff-description"),$list_obj->description,api_text("lists_edit-ff-description-placeholder"),null,null,null,"rows='3'");
 // controls
 $form->addControl("submit",api_text("form-fc-submit"));
 if($list_obj->id){
  $form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=".api_return_script("lists_view")."&idList=".$list_obj->id);
  if(!$list_obj->deleted){
   $form->addControl("button",api_text("form-fc-delete"),"?mod=".MODULE."&scr=submit&act=list_delete&idList=".$list_obj->id,"btn-danger",api_text("lists_edit-fc-delete-confirm"));
  }else{
   $form->addControl("button",api_text("form-fc-undelete"),"?mod=".MODULE."&scr=submit&act=list_undelete&idList=".$list_obj->id,"btn-warning");
   //$form->addControl("button",api_text("form-fc-remove"),"?mod=".MODULE."&scr=submit&act=list_remove&idList=".$list_obj->id,"btn-danger",api_text("lists_edit-fc-remove-confirm"));
  }
 }else{$form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=lists_list");}
 // build grid object
 $grid=new cGrid();
 $grid->addRow();
 $grid->addCol($form->render(),"col-xs-12");
 // add content to html
 $html->addContent($grid->render());
 // renderize html page
 $html->render();
 // debug
 if($GLOBALS['debug']){api_dump($list_obj,"list");}
?>