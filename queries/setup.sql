--
-- Setup module Price Lists
--
-- Version 1.0.0
--

-- --------------------------------------------------------

SET TIME_ZONE = "+00:00";
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET FOREIGN_KEY_CHECKS = 0;

-- --------------------------------------------------------

--
-- Table structure for table `price-lists__lists`
--

CREATE TABLE IF NOT EXISTS `price-lists__lists` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `price-lists__events`
--

CREATE TABLE IF NOT EXISTS `price-lists__events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkList` int(11) unsigned NOT NULL,
  `fkUser` int(11) unsigned DEFAULT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `level` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `fkConnection` (`fkConnection`),
   CONSTRAINT `price-lists__events_ibfk_1` FOREIGN KEY (`fkList`) REFERENCES `price-lists__lists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT INTO `framework__modules_authorizations` (`id`,`module`,`action`) VALUES
(NULL,'price-lists','lists-view'),
(NULL,'price-lists','lists-edit');

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
